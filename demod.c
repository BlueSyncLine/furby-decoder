#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <complex.h>
#include <malloc.h>

// This expects input samples in the format of two-channel, 16-bit signed integers at 44100 Hz.

#define SAMPLE_RATE     44100
#define CLOCK           50
#define SYMBOL_DURATION SAMPLE_RATE / CLOCK

// Filter bandwidth (chosen empirically).
#define FILTER_BANDWIDTH 250.0f

// There's five tones: 0-3 and sync.
#define NUM_TONES 5
#define SYNC_INDEX 4

// Wait for up to a second and a half for the continuation packet.
#define CONTINUATION_TIMEOUT (SAMPLE_RATE*1.5f)

// Tone frequencies.
const float TONE_FREQUENCIES[NUM_TONES] = {16376.0, 16938.0, 18624.0, 18062.0, 17500.0};

// Checksums for the 64 packets.
const char PACKET_CHECKSUMS[64][4] = {
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {0, 2, 1, 0},
    {0, 3, 0, 0},
    {1, 0, 2, 3},
    {1, 1, 3, 3},
    {1, 2, 3, 3},
    {1, 3, 2, 3},
    {0, 1, 2, 0},
    {0, 2, 0, 1},
    {0, 3, 3, 0},
    {1, 0, 2, 1},
    {1, 1, 0, 3},
    {1, 2, 2, 2},
    {1, 3, 1, 3},
    {2, 0, 2, 1},
    {0, 2, 2, 0},
    {0, 3, 3, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 2, 0, 3},
    {1, 3, 1, 3},
    {2, 0, 0, 0},
    {2, 1, 1, 0},
    {0, 3, 0, 0},
    {1, 0, 1, 1},
    {1, 1, 2, 0},
    {1, 2, 0, 1},
    {1, 3, 2, 3},
    {2, 0, 1, 1},
    {2, 1, 2, 0},
    {2, 2, 0, 1},
    {1, 0, 3, 3},
    {1, 1, 2, 3},
    {1, 2, 2, 3},
    {1, 3, 3, 3},
    {2, 0, 3, 3},
    {2, 1, 2, 3},
    {2, 2, 2, 3},
    {2, 3, 3, 3},
    {1, 1, 1, 3},
    {1, 2, 3, 2},
    {1, 3, 0, 3},
    {2, 0, 3, 1},
    {2, 1, 1, 3},
    {2, 2, 3, 2},
    {2, 3, 0, 3},
    {3, 0, 1, 2},
    {1, 2, 1, 3},
    {1, 3, 0, 3},
    {2, 0, 1, 0},
    {2, 1, 0, 0},
    {2, 2, 1, 3},
    {2, 3, 0, 3},
    {3, 0, 3, 3},
    {3, 1, 2, 3},
    {1, 3, 3, 3},
    {2, 0, 0, 1},
    {2, 1, 3, 0},
    {2, 2, 1, 1},
    {2, 3, 3, 3},
    {3, 0, 2, 2},
    {3, 1, 1, 3},
    {3, 2, 3, 2}
};

// The packet footer.
const char PACKET_FOOTER[4] = {1, 0, 3, 2};

// Every packet is 25 symbols long.
#define PACKET_LENGTH 25

// ... of which 12 carry data.
#define PACKET_DATA 12

// The delay line accomodates the duration of one packet.
#define DELAY_LENGTH (PACKET_LENGTH * SYMBOL_DURATION)

// Total samples read.
int t, prev_code, continuation;


// Compute a single-pole IIR decay coefficient for a given bandwidth.
float iir_coeff(float bandwidth) {
    return 1.0f - expf(-2.0f * M_PI * bandwidth / SAMPLE_RATE);
}

// Decode packets with up to one symbol error by correlating them with known ones and counting the errors.
// Returns -1 if no codes match.
int correlate_code(char symbols[PACKET_LENGTH]) {
    int i, code, errors;
    char candidate_symbols[PACKET_DATA];

    // Packet header, bits 11.
    candidate_symbols[0] = 3;

    // Packet footer.
    for (i = 0; i < 4; i++)
        candidate_symbols[8 + i] = PACKET_FOOTER[i];

    // Brute-force the codes and test for correlation.
    for (code = 0; code < 64; code++) {
        // Unpack the code bits.
        candidate_symbols[1] = code >> 4;
        candidate_symbols[2] = (code >> 2) & 3;
        candidate_symbols[3] = code & 3;

        // Insert the checksum.
        for (i = 0; i < 4; i++)
            candidate_symbols[4 + i] = PACKET_CHECKSUMS[code][i];

        errors = 0;
        for (i = 0; i < PACKET_DATA; i++) {
            // Do the two symbols match?
            if (symbols[i * 2 + 1] != candidate_symbols[i]) {
                errors++;

                // No point checking further.
                if (++errors == 2)
                    break;
            }
        }

        // Less than two errors, return the code.
        if (errors < 2)
            return code;
    }

    // No code found.
    return -1;
}

// Try decoding a packet.
void try_decode(char *symbol_delay, int offset) {
    int i, code;
    char symbols[PACKET_LENGTH];

    // Pick the symbols out of the delay line.
    for (i = 0; i < PACKET_LENGTH; i++)
        symbols[i] = symbol_delay[(offset + i * SYMBOL_DURATION) % DELAY_LENGTH];

    // Extract the code, if present.
    if ((code = correlate_code(symbols)) < 0)
        return;

    // Verify the checksum and packet footer.
    for (i = 0; i < 4; i++) {
        if (symbols[9 + i * 2] != PACKET_CHECKSUMS[code][i])
            return;

        if (symbols[17 + i * 2] != PACKET_FOOTER[i])
            return;
    }

    // Continuation
    if (code >= 0x20) {
        if (continuation) {
            fprintf(stderr, "[+] second packet: %d\n", code);

            // Arrange the two 5-bit words into a command code.
            code &= 0x1f;
            code |= prev_code << 5;
            continuation = 0;

            // Output it.
            printf("%d\n", code);
        }

    } else {
        // First part received, set prev_code and continuation.
        if (!continuation) {
            fprintf(stderr, "[+] first packet: %d\n", code);
            prev_code = code;
            continuation = CONTINUATION_TIMEOUT;
        }
    }
}

int main(int argc, char *argv[]) {
    int i, j;
    int16_t samp[2];
    float filter_coeff, amplitude, max_amplitude;
    float complex lo, filter_y[NUM_TONES];

    // Delay line for candidate data.
    char max_tone, *symbol_delay;

    // Try allocating the delay line buffer.
    if (!(symbol_delay = calloc(DELAY_LENGTH, 1))) {
        perror("Couldn't allocate symbol_delay");
        return EXIT_FAILURE;
    }

    // Compute the filter coefficient.
    filter_coeff = iir_coeff(FILTER_BANDWIDTH);

    // Initialize the variables.
    t = 0;
    j = 0;
    continuation = 0;
    for (i = 0; i < NUM_TONES; i++)
        filter_y[i] = 0.0f;

    // While samples are available...
    while (fread(&samp, sizeof(int16_t), 2, stdin)) {
        // Index of the tone with the largest amplitude at the moment.
        max_tone = 0;
        max_amplitude = 0.0f;

        // Update the filter bank and find the peak.
        for (i = 0; i < NUM_TONES; i++) {
            lo = cexpf(-I * TONE_FREQUENCIES[i] * t * M_PI * 2.0f / SAMPLE_RATE);
            filter_y[i] += filter_coeff * (lo * samp[0] - filter_y[i]);
            amplitude = cabsf(filter_y[i]);

            if (amplitude > max_amplitude) {
                max_amplitude = amplitude;
                max_tone = i;
            }
        }

        // Store the peak index.
        symbol_delay[j++] = max_tone;

        // Wrap around.
        if (j == DELAY_LENGTH)
            j = 0;

        // Try to decode the packet.
        try_decode(symbol_delay, j);

        // Update t.
        t++;

        // Timeout before the continuation code.
        if (continuation) {
            continuation--;

            if (!continuation)
                fprintf(stderr, "[!] timed out\n");
        }
    }

    // Free the symbol delay line.
    free(symbol_delay);

    return EXIT_SUCCESS;
}
