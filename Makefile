all: clean demod

clean:
	rm -f demod

demod:
	gcc -Wall -Wextra -Ofast -lm demod.c -o demod
